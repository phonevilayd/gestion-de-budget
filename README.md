Gestion de budget 

Afin de négocier une augmentation à votre travail, ou pour en trouver un nouveau, vous avez décidé de mettre à jour votre portfolio sur gitlab.
Vous avez décidé de coder un tout nouveau projet, qui mettra en valeur l'ensemble de vos compétences.
Pour joindre l'utile à l'agréable, et afin de mieux maîtriser vos dépenses mensuelles, vous avez décidé de vous créer une petite application de gestion de budget que vous allez utiliser dans le cadre de votre foyer.

Analyse des besoins
QUI ?
client : l'ensemble du foyer
utilisateurs : l'ensemble du foyer

pour quel USAGE ?
particuliers, personnel

QUOI ? 
Fonctionalités obligatoires :
- gérer un budget mensuel : entrées et sorties
- les entrées doivent pouvoir être catégorisées (loisirs, famille, loyer/prêt immobilier, etc)
- il doit être possible de "geler" une entrée : une entrée gelée n'est plus modifiable
- l'application doit permettre d'éditer des statistiques mensuelles sur le pourcentage de dépenses par type, l'évolution au fil du temps, etc

Fonctionnalités optionnelles :
-possibilité de gérer un budget commun, et des budgets séparés
-possibilité pour les parents de voir le budget de leurs enfants, mais pas l'inverse
-possibilité de faire un budget prévisionnel, c'est à dire dans le futur, pour planifier des grosses dépenses (vacances, réparation de voiture, achat immobilier, etc)
-possibilité de voir son solde bancaire et son évolution au fil du temps
-possibilité d'ajouter des justificatifs (tickets de caisse, ticket de péage, facture, etc)
-possibilité de désigner une entrée comme étant récurrente (elle est automatiquement ajoutée tous les mois, par exemple votre abonnement à Internet)

COMMENT ?
Contexte final:
appli web 
Techno : Java/MySQL
Délai : sur temps libre

Pour le classDiagram :
 Ici j'ai établi quatre classes pour faire un modèle de données: User, BankAccount, CurrentAccount et SavingAccount.
 Les classes User et BankAccount sont liées en ManyToMany; en effet, un User ou plusieurs(lorsqu'il s'agit d'un compte commun par exemple) peuvent avoir un ou des comptes bancaires.

La classe BankAccount contient aussi les foreign keys idUser, firstName ainsi que lastName


Pour le detailedClassDiagram :
Pareil que pour le diagramme de classes qui sert de modèle de données, ici j'ai édité les classes User & BankAccount; la classe User possède cette fois les méthodes :

- getUser pour récupérer un utilisiteur;
- login pour permettre à l'utilisateur de se connecter;
- register pour permettre de s'inscrire;
- resetPasword pour que l'utilisateur puisse le recrééer si besoin.

Pour la classe BankAccount :

- addExpense pour pouvoir ajouter une dépense;
- updateExpense pour modifier un montant saisi;
- deleteExpense pour supprimer un montant;
- freezeInput pour geler une entrée;
- categorizeInput pour catégoriser les entrées;
- addReceipt pour donner un justificatif;
- setRecurrentInput pour désigner une entrée comme récurrente.

La classe CurrentAccount héritera de la méthode addExpense & sera aussi liée en ManyToMany avec BankAccount.

